# React + TypeScript + Vite

This is a simple web application where the user selects a place (by Name or Lat/Long) and retrieves information regarding weather forecasts.

Using Vite for Learning purposes

- [@vitejs/plugin-react](https://github.com/vitejs/vite-plugin-react/blob/main/packages/plugin-react/README.md) uses [Babel](https://babeljs.io/) for Fast Refresh

Using Leaflet with OpenStreetMap as Map provider. [link-here](https://leafletjs.com/)

## Running

- Clone repository and do the following:

```
  $ cd <project-folder>
  $ npm i
  $ npm run dev
```

- You should see application runing at `localhost:3001`. If you need to change the port, go to `vite.config.ts` and set the disired one there.
- Browser will ask to use your location:
- ![Give Permission](public/browser-location.png)

## APIs used

- Open-meteo - Open-Meteo is an open-source weather API and offers free access for non-commercial use. [link-here](https://www.npmjs.com/package/openmeteo)
- OpenCage - Convert coordinates to and from places. [link-here](https://opencagedata.com/)
  - API key is needed, below is the env variable used for it:
  ```
    VITE_OPEN_CAGE_API_KEY=YOUR_API_KEY
  ```

## Usage

- When application starts, you will see the Add-Forecast page, there you will be able to insert the location and fetch for forecast.
- `Use Your Location` button will be disabled at beginning.
- `Get Weather Forecast` button will be available when conditions to fetch information are met.
- You can navigate to `Forecasts` by a Link in Header or by a button, `Go to Forecasts!` when available.

- App has two routes:
  - `/add-forecast` - application starts here, user can request a new forecast over four methods:
    - Browser location
    - Typing desired location name
    - Entering Latitude and Longitude valid values
    - Selecting a point on Map.
  - `/forecast` - if available, forecast for current time and week will be shwon here.

## Features:

- Application is caching `open-meteo` calls for 24 hours
- Application has two `Zustand` state, one for weather information, other for geocoding.
- Custom hook:
  - useBrowserLocation.tsx - gets, if available user`s browser location information.

## TODO

- Fields and navigation validations.
- Unit tests.
- Reflect Search by Name and Search by Lat-Long features in the Map.
- Implement a better typing for all TypeScript types, by removing `any`'s and other enhancements.
- Implement `i18n` providing translations for user locale.
- Use a CSS pre-processor to increase modularity and code reuse, maybe with `sass`.
- Manage caching through a library like `react-query`.
- Improve layout, possibly adding themes.
