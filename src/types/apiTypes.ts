export type WeatherForecastResponse = {
  temperature: number;
  precipitation: number;
};
