import { WeatherApiResponse } from "@openmeteo/sdk/weather-api-response";

const range = (start: number, stop: number, step: number) =>
  Array.from({ length: (stop - start) / step }, (_, i) => start + i * step);

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const formatWeatherData = (response: WeatherApiResponse) => {
  const utcOffsetSeconds = response.utcOffsetSeconds();

  const current = response.current()!;
  const daily = response.daily()!;

  const weatherData = {
    current: {
      time: new Date((Number(current.time()) + utcOffsetSeconds) * 1000),
      temperature: current.variables(0)!.value(),
      relativeHumidity: current.variables(1)!.value(),
      weatherCode: current.variables(2)!.value(),
    },

    daily: {
      time: range(
        Number(daily.time()),
        Number(daily.timeEnd()),
        daily.interval()
      ).map((t) => new Date((t + utcOffsetSeconds) * 1000)),
      weatherCode: daily.variables(0)!.valuesArray()!,
      temperatureMax: daily.variables(1)!.valuesArray()!,
      temperatureMin: daily.variables(2)!.valuesArray()!,
      precipitation: daily.variables(3)!.valuesArray()!,
    },
  };

  return weatherData;
};
