import { fetchWeatherApi } from "openmeteo";
import { formatWeatherData } from "./helper";

const openMeteoUrl = import.meta.env.VITE_OPEN_METEO_BASE_URL;

const FORECAST_URL = `${openMeteoUrl}/forecast`;

export const fetchWheatherData = async (
  latitude: number = 52.54,
  longitude: number = 13.41
) => {
  const params = {
    latitude,
    longitude,
    current: "temperature_2m,relative_humidity_2m,weather_code",
    daily:
      "weather_code,temperature_2m_max,temperature_2m_min,precipitation_probability_mean",
  };

  const responses = await fetchWeatherApi(FORECAST_URL, params, 0);
  const formattedData = formatWeatherData(responses[0]);

  return formattedData;
};
