import axios from "axios";

const OPEN_CAGE_URL = import.meta.env.VITE_OPEN_CAGE_BASE_URL;
const GEOCODE_API_KEY = import.meta.env.VITE_OPEN_CAGE_API_KEY;

export const geocodeCity = async (locationName: string) => {
  try {
    const response = await axios.get(
      `${OPEN_CAGE_URL}/json?q=${locationName}&key=${GEOCODE_API_KEY}`
    );
    if (response.data.results.length > 0) {
      return response.data.results[0];
    }
  } catch (error) {
    throw new Error("Error fetching city coordinates");
  }
};

export const getPlaceName = async (
  latitude: number,
  longitude: number
): Promise<string | undefined> => {
  try {
    const url = `${OPEN_CAGE_URL}/json?key=${GEOCODE_API_KEY}&q=${latitude}+${longitude}&language=en&pretty=1`;

    const response = await axios.get(url);
    const results = response.data.results;

    if (results && results.length > 0) {
      return results[0];
    } else {
      throw new Error("No results found");
    }
  } catch (error) {
    console.error("Error fetching location:", error);
    return undefined;
  }
};
