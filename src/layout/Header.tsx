import "./header.css";

import { useNavigate } from "react-router-dom";

const Header = () => {
  const navigate = useNavigate();

  return (
    <header>
      <section>
        <a onClick={() => navigate("/add-forecast")}>Add-Forecasts</a>
        <a onClick={() => navigate("/forecasts")}>Forecasts</a>
      </section>
    </header>
  );
};

export default Header;
