import { create } from "zustand";
import { geocodeCity, getPlaceName } from "../service/opencage";

type LatLong = {
  lat: number;
  long: number;
};

type OpencageParams = LatLong | string;

interface GeocodingState {
  isLoading: boolean;
  isError: boolean;
  geocodingData: any[] | null; // Adjust the type according to your API response
  fetchGeocodingData: (params: OpencageParams) => Promise<void>;
}

const useGeocodingStore = create<GeocodingState>((set) => ({
  isLoading: false,
  isError: false,
  geocodingData: null,
  fetchGeocodingData: async (query) => {
    try {
      set({ isLoading: true, isError: false });

      const response =
        typeof query === "string"
          ? await geocodeCity(query)
          : await getPlaceName(query.lat, query.long);

      if (response) {
        set({ geocodingData: response });
      } else {
        set({ geocodingData: [], isError: true });
      }

      set({ isLoading: false });
    } catch (error) {
      console.error("Error fetching geocoding data:", error);
      set({ isLoading: false, isError: true });
    }
  },
}));

export default useGeocodingStore;
