import { create } from "zustand";
import { fetchWheatherData } from "../service/openmeteo";

interface CacheItem {
  data: any; // Adjust the type according to your API response
  timestamp: number;
}

interface Cache {
  [key: string]: CacheItem;
}

interface WeatherStore {
  data: any;
  cache: Cache;
  loading: boolean;
  fetchData: (lat?: number, long?: number) => Promise<void>;
  fetchDataManually: (lat?: number, long?: number) => void;
}

const useWeatherStore = create<WeatherStore>((set) => {
  const initialCache = JSON.parse(localStorage.getItem("apiCache") || "{}");

  return {
    cache: initialCache,
    data: null,
    loading: false,
    fetchData: async (lat?: number, long?: number) => {
      try {
        set({ loading: true });

        const cacheKey = JSON.stringify({ lat, long });

        // Check if data is already cached and not expired
        if (initialCache[cacheKey]) {
          const { data, timestamp } = initialCache[cacheKey];
          const currentTime = new Date().getTime();
          if (currentTime - timestamp < 24 * 60 * 60 * 1000) {
            set({ loading: false });
            set({ data });
            return data;
          }
        }

        const weatherData = await fetchWheatherData(lat, long);

        // Cache the response along with the timestamp
        const currentTime = new Date().getTime();
        const cacheData: CacheItem = {
          data: weatherData,
          timestamp: currentTime,
        };
        const newCache = { ...initialCache, [cacheKey]: cacheData };

        // Update the cache in localStorage
        localStorage.setItem("apiCache", JSON.stringify(newCache));

        set({ data: weatherData, loading: false });
      } catch (error) {
        console.error("Error fetching data:", error);
        set({ loading: false });
      }
    },

    // New action to manually trigger data fetch
    fetchDataManually: (lat?: number, long?: number) => {
      set({ loading: true });
      set({ data: null });
      useWeatherStore.getState().fetchData(lat, long);
    },
  };
});

export default useWeatherStore;
