import App from "../App";
import AddForecast from "../pages/add-forecast/AddForecast";
import Forecasts from "../pages/forecast/Forecasts";
import { Routes, Route } from "react-router-dom";

function AppRoutes() {
  return (
    <Routes>
      <Route path="/forecasts" Component={Forecasts} />
      <Route path="/add-forecast" Component={AddForecast} />
      <Route path="/*" Component={AddForecast} />
    </Routes>
  );
}

export default AppRoutes;
