/* eslint-disable @typescript-eslint/no-explicit-any */
import React, { useEffect, useState } from "react";
import MapComponent from "../../components/map/Map";
import useWeatherStore from "../../store/weatherStore";

import "./Add-Forecast.css";
import useBrowserLocation from "../../hooks/useBrowserLocation";
import { useNavigate } from "react-router-dom";
import { formatGeolocation } from "../../utils/helpers";
import useGeocodingStore from "../../store/geocodingStore";

const AddForecast: React.FC = () => {
  const navigate = useNavigate();

  const { data, loading, fetchDataManually } = useWeatherStore();
  const { geocodingData, fetchGeocodingData } = useGeocodingStore();
  const { location, loading: lodingBrowserLocation } = useBrowserLocation();

  const [name, setName] = useState<string>();
  const [inputLatLong, setInputLatLong] = useState<{
    inputLat?: number | "";
    inputLong?: number | "";
  }>();
  const [lat, setLat] = useState<number>();
  const [long, setLong] = useState<number>();

  useEffect(() => {
    if (geocodingData) {
      const { lat, lng } = geocodingData?.geometry;
      setInputLatLong({ inputLat: "", inputLong: "" });

      setLat(lat);
      setLong(lng);
    }
  }, [geocodingData]);

  const handleLocationClick = (lat: number, long: number) => {
    setLat(lat);
    setLong(long);
    fetchGeocodingData({ lat, long });
  };

  const handleSearch = () => {
    fetchDataManually(lat, long);

    if (!loading && data && data !== null) {
      navigate("/forecasts");
    }
  };

  const setCurrentLocation = () => {
    if (location) {
      setInputLatLong({ inputLat: "", inputLong: "" });
      setName("");
      setLat(location.latitude);
      setLong(location.longitude);
      fetchGeocodingData({ lat: location.latitude, long: location.longitude });
    }
  };

  const handleNameInput = (e: { target: { value: any } }) => {
    const locationName = e.target.value;
    setName(locationName);
  };

  const handleLatLngChange = (e: any) => {
    setInputLatLong({
      ...inputLatLong,
      [e.target.name]: e.target.value,
    });
  };

  const setCoordsOnMap = () => {
    setName("");
    inputLatLong &&
      handleLocationClick(
        inputLatLong.inputLat! as number,
        inputLatLong.inputLong! as number
      );
  };

  return (
    <div>
      <section className="search-mode">
        <h1>Add Forecast:</h1>

        <div className="search-mode__content">
          <div>
            <h2>By using your location</h2>
            <div className="search-mode__method">
              <button
                disabled={lodingBrowserLocation}
                onClick={setCurrentLocation}
              >
                Use your current location!
              </button>
            </div>
          </div>

          <div>
            <h2>By location name</h2>
            <div className="search-mode__method">
              <div>
                <label htmlFor="input-name">Name: </label>
                <input
                  id="input-name"
                  type="text"
                  value={name}
                  onChange={handleNameInput}
                />
              </div>
            </div>
            <button disabled={!name} onClick={() => fetchGeocodingData(name!)}>
              Set Coords
            </button>
          </div>

          <div>
            <h2>By lat/long</h2>
            <div className="search-mode__method">
              <div>
                <label htmlFor="input-lat">Lat: </label>
                <input
                  id="input-lat"
                  name="inputLat"
                  type="number"
                  value={inputLatLong?.inputLat}
                  onChange={handleLatLngChange}
                />
              </div>
              <div>
                <label htmlFor="input-long">Long: </label>
                <input
                  id="input-long"
                  name="inputLong"
                  type="number"
                  value={inputLatLong?.inputLong}
                  onChange={handleLatLngChange}
                />
              </div>
            </div>
            <button
              disabled={!inputLatLong?.inputLat || !inputLatLong?.inputLong}
              onClick={setCoordsOnMap}
            >
              Set Coords
            </button>
          </div>
        </div>

        <h2>By map picker</h2>
        <div id="map" className="map-container">
          <MapComponent onLocationClick={handleLocationClick} />
        </div>
      </section>

      <hr />

      <section className="row-section">
        <h2>Selected location:</h2>
        <h2>Lat: {lat}</h2>
        <h2>Long: {long}</h2>
      </section>

      <div>
        <h2>{geocodingData && formatGeolocation(geocodingData).formatted}</h2>
      </div>

      <section className="row-section">
        <button disabled={!lat || !long} onClick={handleSearch}>
          Get Weather Forecast
        </button>
      </section>

      <section className="row-section">
        {loading && <div>Loading data...</div>}
        {!loading && data && data !== null && (
          <button onClick={() => navigate("/forecasts")}>
            Go to Forecasts!
          </button>
        )}
      </section>
    </div>
  );
};

export default AddForecast;
