import React, { useEffect, useMemo } from "react";
import useWeatherStore from "../../store/weatherStore";
import { useNavigate } from "react-router-dom";

import "./Forecasts.css";
import Card from "../../components/card/Card";
import { formatGeolocation, getCachedLatAndLong } from "../../utils/helpers";
import useGeocodingStore from "../../store/geocodingStore";

const Forecasts: React.FC = () => {
  const { data, cache, fetchData } = useWeatherStore();
  const { geocodingData, fetchGeocodingData } = useGeocodingStore();
  const navigate = useNavigate();

  useEffect(() => {
    if (!data) {
      const { lat, long } = getCachedLatAndLong();
      fetchData(lat, long);
    }
  }, [data, cache, fetchData]);

  const formattedLocation = useMemo(() => {
    if (geocodingData) {
      return formatGeolocation(geocodingData);
    } else {
      const cachedLatLong = getCachedLatAndLong();
      if (cachedLatLong)
        fetchGeocodingData({
          lat: cachedLatLong.lat,
          long: cachedLatLong.long,
        });
    }
  }, [fetchGeocodingData, geocodingData]);

  const currentWeather = useMemo(() => {
    if (data) {
      const { current } = data;
      return {
        time: new Date(current.time).toLocaleDateString(),
        temperature: current.temperature.toFixed(2),
        relativeHumidity: current.relativeHumidity,
        weatherCode: current.weatherCode,
      };
    }
  }, [data]);

  const weeklyWeather = useMemo(() => {
    const formattedWeather: any[] = [];
    if (data) {
      const { daily } = data;
      const { temperatureMax, temperatureMin, precipitation, time } = daily;
      for (let i = 0; i < time.length; i++) {
        formattedWeather.push({
          date: new Date(time[i]),
          min: (temperatureMin[i] as number).toFixed(2),
          max: (temperatureMax[i] as number).toFixed(2),
          precipitation: precipitation[i].toFixed(2),
        });
      }
    }

    return formattedWeather;
  }, [data]);

  return (
    <section className="forecasts">
      {data ? (
        <>
          {formattedLocation && (
            <h1>{`${formattedLocation.city} - ${formattedLocation.country} ${formattedLocation.flag}`}</h1>
          )}

          <div className="current">
            Weather Now: {currentWeather?.time}
            <div>Temperature: {currentWeather?.temperature}º</div>
            <div>Humidity: {currentWeather?.relativeHumidity} %</div>
            <div>Weather Code: {currentWeather?.weatherCode}</div>
            <div></div>
          </div>
          <hr />

          <div>
            Week Forecast:
            <div className="week">
              {weeklyWeather.map((element, idx) => (
                <Card key={idx} content={element} />
              ))}
            </div>
          </div>
        </>
      ) : (
        <div>
          No data available yet!{" "}
          <span onClick={() => navigate("/add-forecast")}>
            Click here to fetch!
          </span>
        </div>
      )}
    </section>
  );
};

export default Forecasts;
