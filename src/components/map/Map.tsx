/* eslint-disable @typescript-eslint/no-explicit-any */
// MapComponent.tsx
import React, { useRef, useEffect, useState } from "react";

import { MapContainer, TileLayer, useMapEvents, Marker } from "react-leaflet";
import useBrowserLocation from "../../hooks/useBrowserLocation";
import { LatLngExpression } from "leaflet";

import "./Map.css";

interface MapProps {
  onLocationClick: (lat: number, lon: number) => void;
}

const defaultCenter: LatLngExpression = [0, 0];

const MapComponent: React.FC<MapProps> = ({ onLocationClick }) => {
  const mapRef = useRef<any>(null);
  const [clickedLocation, setClickedLocation] = useState<
    [number, number] | null
  >(null);

  const { location } = useBrowserLocation();

  const handleMapClick = (event: any) => {
    const { latlng } = event;
    const { lat, lng } = latlng;
    setClickedLocation([lat, lng]);
    onLocationClick(lat, lng);
  };

  useEffect(() => {
    if (mapRef.current && location) {
      const map = mapRef.current;
      map.setView([location.latitude, location.longitude], map.getZoom());
    }
  }, [location]);

  // Center the map on the clicked location
  useEffect(() => {
    if (mapRef.current && clickedLocation) {
      const map = mapRef.current;
      map.setView(clickedLocation, map.getZoom());
    }
  }, [clickedLocation, location]);

  return (
    <MapContainer
      center={
        location ? [location.latitude, location.longitude] : defaultCenter
      }
      zoom={6}
      className="map-container"
      ref={mapRef}
    >
      <TileLayer
        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
        attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
      />

      {!clickedLocation && location && (
        <Marker position={[location.latitude, location.longitude]} />
      )}

      <MapClickHandler onClick={handleMapClick} />
      {clickedLocation && <Marker position={clickedLocation} />}
    </MapContainer>
  );
};

interface MapClickHandlerProps {
  onClick: (event: any) => void;
}

const MapClickHandler: React.FC<MapClickHandlerProps> = ({ onClick }) => {
  useMapEvents({
    click: onClick,
  });

  return null;
};

export default MapComponent;
