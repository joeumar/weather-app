import React from "react";

import "./card.css";
import { areDatesSameDay } from "../../utils/helpers";

type CardProps = {
  content: {
    date: Date;
    min: number;
    max: number;
    precipitation: number;
  };
};

const Card: React.FC<CardProps> = ({ content }) => {
  const { date, min, max, precipitation } = content;
  const isCurrentDay = areDatesSameDay(date, new Date());

  return (
    <div className={`card ${isCurrentDay ? "current" : ""}`}>
      <h2 className="card-title">{date.toLocaleDateString()}</h2>
      <p className="card-content">Min: {min}º</p>
      <p className="card-content">Max: {max}º</p>
      <p className="card-content">Precipitation: {precipitation}%</p>
    </div>
  );
};

export default Card;
