/* eslint-disable @typescript-eslint/no-explicit-any */
import { useState, useEffect } from "react";

const useBrowserLocation = () => {
  const [location, setLocation] = useState<{
    latitude: number;
    longitude: number;
  } | null>(null);
  const [error, setError] = useState<string | null>(null);
  const [loading, setloading] = useState<boolean>(true);

  useEffect(() => {
    setloading(true);
    const getLocation = () => {
      if (!navigator.geolocation) {
        setError("Geolocation is not supported by your browser");
        setloading(false);
        return;
      }

      const successHandler = (position: any) => {
        setloading(false);

        setLocation({
          latitude: position.coords.latitude,
          longitude: position.coords.longitude,
        });
      };

      const errorHandler = (error: any) => {
        setloading(false);

        setError(error.message);
      };

      navigator.geolocation.getCurrentPosition(successHandler, errorHandler);
    };

    getLocation();
  }, []);

  return { location, loading, error };
};

export default useBrowserLocation;
