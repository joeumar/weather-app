export const areDatesSameDay = (date1: Date, date2: Date) => {
  return (
    date1.getFullYear() === date2.getFullYear() &&
    date1.getMonth() === date2.getMonth() &&
    date1.getDate() === date2.getDate()
  );
};

export const formatGeolocation = (locationOpencage: any) => {
  const { formatted, components, annotations } = locationOpencage;
  const { city, country } = components;
  return {
    formatted,
    city,
    country,
    flag: annotations.flag,
  };
};

export const getCachedLatAndLong = () => {
  return JSON.parse(
    Object.keys(JSON.parse(localStorage.getItem("apiCache")!))[0]
  );
};
